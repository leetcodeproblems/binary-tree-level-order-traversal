//BFS with 1 Queue and loop
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        
        List<List<Integer>> ans = new ArrayList<>();
        
        if(root==null) return ans;
        
        Queue<TreeNode> queue = new LinkedList<>();
        
        queue.add(root);

        while(!queue.isEmpty()){
            
            int size = queue.size();
            List<Integer> temp = new ArrayList<>();
            
            for(int i=0; i<size; i++){
                
                //front
                TreeNode front = queue.poll();
                temp.add(front.val);
                
                ///Neighbors
                if(front.left != null ){
                    queue.add(front.left);
                }
                
                if(front.right != null){
                    queue.add(front.right);
                }
            }
            //level over
            ans.add(temp);
        }
        return ans;
    }
}
