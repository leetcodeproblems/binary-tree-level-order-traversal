//BFS with 2 Queues
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        
        List<List<Integer>> ans = new ArrayList<>();
        
        if(root==null) return ans;
        
        Queue<TreeNode> q = new LinkedList<>();
        Queue<TreeNode> p = new LinkedList<>();
        
        q.add(root);
        
        List<Integer> temp= new ArrayList<>();
        
        while(!q.isEmpty()){
            //front
            TreeNode front= q.remove();
            temp.add(front.val);
            
            ///Neighbors
            if(front.left!=null){
                p.add(front.left);
            }
            if(front.right!=null){
                p.add(front.right);
            }
            
            //level over
            if(q.isEmpty()){
                ans.add(temp);
                q=p;
                p= new LinkedList<>();
                temp= new ArrayList<>();
            }
        }
        return ans;
    }
}
